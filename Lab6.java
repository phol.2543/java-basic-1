
public class Lab6 {

	public static void Repeat(int article) {
		if (article == 1) {
			int i = 1;
			while (i <= 10) {
				System.out.print(" " + i++ + " ");
			}
		}
		if (article == 2) {
			int i = 1;
			int sum = 1;
			while (i <= 9) {
				i++;
				sum = sum + i;

			}
			System.out.print("Sum 1 - 10 = " + sum);
		}

		if (article == 3) {
			int i = 1;
			System.out.println("Number 1 - 100 Devided by 12  : ");
			while (i <= 100) {
				if (i % 12 == 0) {
					System.out.print(i + "  ");
				}
				i++;
			}
		}

		if (article == 4) {
			int array[] = { 1, 2, 3, 4, 5, };
			int i = 0;
			System.out.println("Value in array : ");
			while (i < array.length) {
				System.out.print(array[i] + "  ");
				i++;
			}

		}
	}

	public static void main(String[] args) {
		System.out.println("No. 1");
		Repeat(1);

		System.out.println("\n\nNo. 2");
		Repeat(2);

		System.out.println("\n\nNo. 3");
		Repeat(3);

		System.out.println("\n\nNo. 4");
		Repeat(4);

	}

}
