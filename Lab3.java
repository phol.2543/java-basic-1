
public class Lab3 {
	public static int posposdede(Boolean posORde) {
		int i = 20;
		for(int j = 0 ; j<5 ; j++) {
			if(posORde == true) {
				i++;
			}
			if(posORde == false) {
				i--;
			}
		}
		
		return i;
	}
	
	public static void main(String[] args) {
		
		System.out.println("I++ "+posposdede(true));
		System.out.println("I-- "+posposdede(false));
		
		System.out.println("---------------------------------");
		float F1 = 1.5f ;
		float F2 = 2.5f ;
		if(F1 < F2 && F2 > F1) {
			System.out.println("Test && float > <");
		}
		if(F1==1.5 && F2== 2.5) {
			System.out.println("Test && float == ");
		}
		
		System.out.println("----------------------------------");
		char C1 = '1';
		char C2 = '2' ;
		if(C1==C2 || C2== '2') {
			System.out.println("Test || char ");
		}
		
	 }
}
