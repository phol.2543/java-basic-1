
public class Lab13 {

	public static void main(String[] args) {
		int Array[][] = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9, 10 } };

		for (int i = 0; i < Array.length; i++) {
			for (int j = 0; j < Array[i].length; j++) {
				System.out.println("Array row " + (i + 1) + " column " + (j + 1) + " = " + Array[i][j]);
			}
		}

	}

}
